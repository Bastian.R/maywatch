
# Maywatch

Application web responsive pour rechercher des séries et obtenir des recommandations.

**Link :** https://maywatch.vercel.app/



## Features

- Recherche d'une série.
- Affichage d'une série avec son titre, sa note, sa description.
- Recommandation de diverses séries en lien avec celle recherchée.
- Chargement des informations asynchrones.



## Tech Stack

React, JS, TailwindCSS, Font Awesome, CSS, Fetch, Api themoviedb, Jest, React testing library.


## Running Tests

Cette application web intègre des tests unitaires pour les composants React et d'intégration pour les requêtes asynchrones.

Pour démarrer les tests : 

```bash
  npm run test
```

Pour démarrer le coverage des tests :

```bash
  npm run test:cov
```

Current total statements covered : 81.11%

![Coverage](https://i.postimg.cc/L5cT33Pf/coverage.png)

## Screenshot
![Maywatch riverdale desktop](https://i.postimg.cc/T2HtyLQF/maywatch-desktop-0.png)
![Maywatch riverdale tablet](https://i.postimg.cc/j5WCjFMD/maywatch-tablet.png)
![Maywatch riverdale mobile](https://i.postimg.cc/cJCRNZ8Y/maywatch-mobile.png)
![The mandalorian riverdale desktop](https://i.postimg.cc/T3RZgmbK/the-mandalorian-maywatch-desktop.png)
![The mandalorian riverdale tablet](https://i.postimg.cc/0QmtL2mg/the-mandalorian-maywatch-table.png)
![The mandalorian mobile](https://i.postimg.cc/zBSCCLKf/the-mandalorian-maywatch-phone.png)



## Authors

- [@Bastian.R](https://gitlab.com/Bastian.R/)

