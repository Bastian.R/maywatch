const BASE_URL = "https://api.themoviedb.org/3";
const API_KEY_PARAM = "?api_key=e441d00afa97da6b555a20dd7c2c2af5";
const BACKDROP_BASE_URL = "https://image.tmdb.org/t/p/original";
const SMALL_IMG_COVER_BASE_URL = "https://image.tmdb.org/t/p/w300/";
const ORIGIN_COUNTRY = "&with_origin_country=US|FR|UK|SK";
const WATCH_PROVIDER = "&with_watch_providers=8";
const WATCH_REGION = "&watch_region=FR";
const POPULARITY_PARAM = "&sort_by=popularity.desc";
const INCLUDE_ADULT = "&include_adult=true";

export {
  BASE_URL,
  API_KEY_PARAM,
  BACKDROP_BASE_URL,
  SMALL_IMG_COVER_BASE_URL,
  ORIGIN_COUNTRY,
  POPULARITY_PARAM,
  INCLUDE_ADULT,
  WATCH_PROVIDER,
  WATCH_REGION,
};
