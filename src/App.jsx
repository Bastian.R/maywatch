import "./global.css";
import { TvShowAPI } from "./api/tv-show.js";
import { useEffect, useState } from "react";
import { BACKDROP_BASE_URL } from "./config.js";
import { TvShowDetail } from "./components/TvShowDetail/TvShowDetail";
import { Logo } from "./components/Logo/Logo";
import { TvShowListItem } from "./components/TvShowListItem/TvShowListItem";
import logo from "./assets/images/logo.png";
import { SearchBar } from "./components/SearchBar/SearchBar";

function App() {
  const [currentTvShow, setCurrentTvShow] = useState();
  const [currentRecommendations, setCurrentRecommendations] = useState([]);

  function backStartHorizontalScroll() {
    const recommendations = document.querySelector(".recommendations-list");
    const recommendationsScrollWidth = recommendations.scrollWidth;
    recommendations.scrollLeft -= recommendationsScrollWidth;
  }

  function setCurrentTvShowChild(tvShow) {
    setCurrentTvShow(tvShow);
    backStartHorizontalScroll();
  }

  function setCurrentTvShowChildBySearch(event) {
    if (event.key == "Enter" && event.target.value.trim().length > 0) {
      TvShowAPI.fetchTvShowByTitle(event.target.value).then(
        (result) => {
          setCurrentTvShow(result.results[0]);
          event.target.value = "";
        },
        (rejected) => {
          alert(rejected.message);
        }
      );
    }
  }

  function getPopularTvShow() {
    TvShowAPI.getPopular().then(
      (result) => {
        if (result && result?.results.length > 0) {
          setCurrentTvShow(result.results[0]);
        }
      },
      (rejected) => {
        alert(rejected.message);
      }
    );
  }

  function getRecommendationsTvShowById(tvShowId) {
    TvShowAPI.getRecommendationsById(tvShowId).then(
      (result) => {
        if (result && result?.results.length > 0) {
          setCurrentRecommendations(result.results);
        }
      },
      (rejected) => {
        alert(rejected.message);
      }
    );
  }

  useEffect(() => {
    getPopularTvShow();
  }, []);

  useEffect(() => {
    if (currentTvShow) {
      getRecommendationsTvShowById(currentTvShow.id);
    }
  }, [currentTvShow]);

  return (
    <div
      className="main-container flex flex-col min-h-screen"
      data-testid="main-container-test"
      style={{
        background: currentTvShow
          ? `linear-gradient(rgba(0,0,0,0.55), rgba(0,0,0,0.55)), url("${BACKDROP_BASE_URL}${currentTvShow.backdrop_path}") no-repeat center / cover`
          : "black",
      }}
    >
      <div data-testid="header-test" className="header flex w-full">
        <nav className="header-navigation flex flex-wrap w-full items-center xl:justify-start justify-center">
          <div className="logo h-28 w-[340px] p-6">
            <Logo
              image={logo}
              title="Maywatch"
              subtitle="Find a show you may watch."
            />
          </div>
          <SearchBar
            setCurrentTvShowChildBySearch={setCurrentTvShowChildBySearch}
          />
        </nav>
      </div>

      <div className="tv-show flex w-full grow-[2]">
        {currentTvShow ? (
          <TvShowDetail tvShow={currentTvShow} />
        ) : (
          <span className="waiting-title h-fit w-fit text-white tracking-wider text-2xl font-semibold p-4 pb-[150px]">
            Waiting to get most current popular tv show. 📺
          </span>
        )}
      </div>

      {currentRecommendations.length > 0 ? (
        <TvShowListItem
          currentTvShowGenre={currentTvShow?.genre_ids}
          onClick={setCurrentTvShowChild}
          recommendations={currentRecommendations}
        />
      ) : (
        <span className="waiting-title h-fit w-fit text-white tracking-wider text-2xl font-semibold p-4 pb-[150px]">
          Waiting to get best recommendations.✅
        </span>
      )}
    </div>
  );
}

export default App;
