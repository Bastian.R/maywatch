export function SearchBar({ setCurrentTvShowChildBySearch }) {
  return (
    <div
      data-testid="searchBar-test"
      className="SearchBar w-full xl:absolute flex grow justify-center p-4 xl:p-6"
    >
      <input
        data-testid="searchBar-input-test"
        onKeyUp={(e) => {
          setCurrentTvShowChildBySearch(e);
        }}
        className="searchbar w-[600px] h-12 bg-white rounded-lg p-4"
        placeholder="Search a film you may like .."
      ></input>
    </div>
  );
}
