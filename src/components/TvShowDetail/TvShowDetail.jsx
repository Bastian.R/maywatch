import { StarsRating } from "../StarsRating/StarsRating";

export function TvShowDetail(props) {
  let rating = props.tvShow?.vote_average / 2;
  rating = rating.toFixed(2);
  return (
    <section
      data-testid="tvShowDetail-test"
      className="tv-show-details flex flex-col gap-y-4 justify-center p-4 w-[100%] xl:w-[90%]"
    >
      <span className="tv-show-details-title text-white w-fit text-3xl uppercase text-gray-950 font-semibold tracking-wide">
        {props.tvShow?.name}
      </span>
      <div className="tv-show-details-rating w-fit text-white font-medium tracking-wider text-xl lg:text-2xl font-semibold text-gray-950">
        <StarsRating rating={rating} />
      </div>
      <p className="tv-show-details-paragraph w-full text-white font-medium text-justify text-base lg:text-lg tracking-wide text-gray-950">
        {props.tvShow?.overview}
      </p>
    </section>
  );
}
