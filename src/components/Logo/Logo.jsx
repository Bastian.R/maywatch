export function Logo({ image, subtitle }) {
  return (
    <div
      data-testid="logo-test"
      className="flex flex-col items-center xl:items-start"
    >
      <img alt="logo" src={image} className="image" />
      <span className="logo-subtitle text-white text-lg tracking-wide font-semibold pt-2">
        {subtitle}
      </span>
    </div>
  );
}
