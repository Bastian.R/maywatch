export function StarsRating({ rating }) {
  let starsList = [];
  const starFillCount = Math.floor(rating);
  const hasStarHalf = rating - starFillCount >= 0.5;
  const emptyStarCount = 5 - starFillCount - (hasStarHalf ? 1 : 0);

  for (let i = 0; i < starFillCount; i++) {
    starsList.push(
      <i
        key={"rating-star-" + i}
        data-testid={"rating-star-" + i}
        className="rating-star fas fa-star"
        style={{ color: "white" }}
      ></i>
    );
  }

  hasStarHalf &&
    starsList.push(
      <i
        key="star-half"
        data-testid={"rating-star-half"}
        className="rating-star fa-regular fa-star-half-stroke"
        style={{ color: "white" }}
      ></i>
    );

  for (let i = 0; i < emptyStarCount; i++) {
    starsList.push(
      <i
        key={"star-empty" + i}
        data-testid={"rating-star-empty-" + i}
        style={{ color: "white" }}
        className="fa-regular fa-star"
      ></i>
    );
  }

  return (
    <div className="StarsRating">
      {starsList} {rating} / 5
    </div>
  );
}
