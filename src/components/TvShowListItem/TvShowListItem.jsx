import { SMALL_IMG_COVER_BASE_URL } from "../../config.js";
import imgNoPreview from "../../assets/images/no-preview.jpg";

export function TvShowListItem({
  recommendations,
  onClick,
  currentTvShowGenre,
}) {
  let recommendationsList = [];
  let i = 0;
  let mostPreciseRecommendations = JSON.parse(JSON.stringify(recommendations));
  let preciseRecommendations = JSON.parse(JSON.stringify(recommendations));
  let lessPreciseRecommendations = JSON.parse(JSON.stringify(recommendations));

  function scale(element) {
    const parentOfElement = element.parentElement;
    parentOfElement.style.transform = "scale(1.05)";
  }

  function unScale(element) {
    const parentOfElement = element.parentElement;
    parentOfElement.style.transform = "Scale(1)";
  }

  function deleteDouble(table) {
    const set = new Set();

    table.map((item) => {
      if (!set.has(item.id)) {
        set.add(item.id);
        set.add(item);
      }
    });

    table = set;
    table = [...table];
    return table.filter((tvShow) => tvShow instanceof Object);
  }

  function limitRecommendations(table) {
    table = table.slice(0, 12);
    return table;
  }

  mostPreciseRecommendations = mostPreciseRecommendations.filter((tvShow) => {
    return (
      tvShow.genre_ids[0] == currentTvShowGenre[0] &&
      tvShow.genre_ids.length < 3
    );
  });

  preciseRecommendations = preciseRecommendations.filter((tvShow) => {
    return (
      tvShow.genre_ids[1] == currentTvShowGenre[0] &&
      tvShow.genre_ids.length < 3
    );
  });

  if (currentTvShowGenre.length > 1) {
    lessPreciseRecommendations = lessPreciseRecommendations.filter((tvShow) => {
      if (
        tvShow.genre_ids[0] == currentTvShowGenre[1] &&
        tvShow.genre_ids.length < 3
      ) {
        return tvShow;
      }
    });

    lessPreciseRecommendations = lessPreciseRecommendations.slice(0, 5);

    recommendations = [
      ...mostPreciseRecommendations,
      ...preciseRecommendations,
      ...lessPreciseRecommendations,
    ];

    recommendations = deleteDouble(recommendations);
    recommendations = limitRecommendations(recommendations);
  } else {
    recommendations = [
      ...mostPreciseRecommendations,
      ...preciseRecommendations,
    ];

    recommendations = deleteDouble(recommendations);
    recommendations = limitRecommendations(recommendations);
  }

  recommendations.map((tvShow) => {
    const tvShowItem = (
      <div
        key={`recommendations-tv-show-item-${i}`}
        data-testid={`recommendations-tv-show-item-${i}`}
        className={`recommendations-tv-show-item-${i} shrink-0 h-fit bg-black text-white rounded-md flex flex-col items-center cursor-pointer`}
        onClick={() => onClick(tvShow)}
        onMouseOver={(event) => scale(event.target)}
        onMouseLeave={(event) => unScale(event.target)}
      >
        <img
          className="w-full w-[300px] h-[168.99px]"
          alt={`tv-show-cover-${i}`}
          src={
            tvShow.backdrop_path
              ? SMALL_IMG_COVER_BASE_URL + tvShow.backdrop_path
              : imgNoPreview
          }
        />
        <span className="title text-center truncate w-full p-2">
          {tvShow.name}
        </span>
      </div>
    );
    recommendationsList.push(tvShowItem);
    i++;
  });

  return (
    <div
      data-testid="recommendations"
      className="recommendations flex flex-col gap-y-4 pt-4 pb-4"
    >
      <span className="recommendation-title h-fit w-fit text-white tracking-wider text-2xl font-semibold pl-4">
        You will also like 🔥
      </span>
      <div
        data-testid="recommendations-list"
        className="recommendations-list flex gap-x-10 overflow-hidden pl-4 pr-4"
      >
        {recommendationsList}
      </div>
    </div>
  );
}
