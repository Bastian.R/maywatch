import {
  BASE_URL,
  API_KEY_PARAM,
  BACKDROP_BASE_URL,
  SMALL_IMG_COVER_BASE_URL,
  ORIGIN_COUNTRY,
  POPULARITY_PARAM,
  INCLUDE_ADULT,
  WATCH_PROVIDER,
  WATCH_REGION,
} from "../config.js";

export class TvShowAPI {
  static getPopular() {
    return fetch(
      `${BASE_URL}/discover/tv${API_KEY_PARAM}${ORIGIN_COUNTRY}${POPULARITY_PARAM}${INCLUDE_ADULT}${WATCH_PROVIDER}${WATCH_REGION}`
    )
      .then((response) => {
        if (response.status !== 200) {
          throw new Error(
            `Fetch popular request failed. Request failed : ${response.status}`
          );
        }
        return Promise.resolve(response.json());
      })
      .catch((error) => {
        return Promise.reject(error);
      });
  }

  static getRecommendationsById(tvShowId) {
    return fetch(`${BASE_URL}/tv/${tvShowId}/recommendations${API_KEY_PARAM}`)
      .then((response) => {
        if (response.status !== 200) {
          throw new Error(
            `Fetch recommendations request failed. Request failed : ${response.status}`
          );
        }
        return response.json();
      })
      .then((result) => {
        if (result.results.length > 0) {
          return Promise.resolve(result);
        }
        throw new Error(
          "Fetch recommendations request failed. We didn't find recommendations on our database."
        );
      })
      .catch((error) => {
        return Promise.reject(error);
      });
  }

  static fetchTvShowByTitle(tvShowTitle) {
    return fetch(`${BASE_URL}/search/tv${API_KEY_PARAM}&query=${tvShowTitle}`)
      .then((response) => {
        if (response.status !== 200) {
          throw new Error(
            `Fetch tvShowByTitle request failed. Request failed : ${response.status}`
          );
        }
        return response.json();
      })
      .then((result) => {
        if (result.results.length > 0) {
          return Promise.resolve(result);
        }
        throw new Error(
          "Fetch tvShowByTitle request failed. We didn't find your show on our database."
        );
      })
      .catch((error) => {
        return Promise.reject(error);
      });
  }
}
