import { render, screen, fireEvent } from "@testing-library/react";
import { SearchBar } from "../components/SearchBar/SearchBar";

describe("SearchBar", () => {
  it("Search bar render with possibility to tape our film and search it by pressing enter key", () => {
    const setCurrentTvShowChildBySearch = jest.fn();
    render(
      <SearchBar
        setCurrentTvShowChildBySearch={setCurrentTvShowChildBySearch}
      />
    );
    const input = screen.getByPlaceholderText("Search a film you may like ..");
    input.value = "Blacklist";
    fireEvent.keyUp(input, { key: "Enter", code: "13" });
    expect(setCurrentTvShowChildBySearch).toHaveBeenCalledTimes(1);
  });
});
