import { render, screen } from "@testing-library/react";
import { TvShowDetail } from "../components/TvShowDetail/TvShowDetail";

describe("TvShowDetail", () => {
  it("renders tv show details correctly", () => {
    const tvShow = {
      original_name: "Stranger Things",
      vote_average: 8.3,
      overview:
        "When a young boy disappears, his mother, a police chief, and his friends must confront terrifying supernatural forces in order to get him back.",
    };

    render(<TvShowDetail tvShow={tvShow} />);

    const titleElement = screen.getByText("Stranger Things");
    const ratingElement = screen.getByText("4.15 / 5");
    const overviewElement = screen.getByText(/When a young boy disappears,/i);
  });
});
