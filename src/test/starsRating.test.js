import { render, screen } from "@testing-library/react";
import { StarsRating } from "../components/StarsRating/StarsRating";

describe("Stars rating", () => {
  it("renders 5 stars where filling depend on 3.5 rating", () => {
    const rating = 3.5;

    render(<StarsRating rating={rating} />);

    const stars = screen.getAllByTestId(/rating-star-[0-9]{1}/);
    const starsHalf = screen.getAllByTestId("rating-star-half");
    const starsEmpty = screen.getAllByTestId(/rating-star-empty-[0-9]{1}/);

    expect(Object.keys(stars).length).toBe(3);
    expect(Object.keys(starsHalf).length).toBe(1);
    expect(Object.keys(starsEmpty).length).toBe(1);
    const ratingElement = screen.getByText("3.5 / 5");
  });
});
