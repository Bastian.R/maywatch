import { render, screen } from "@testing-library/react";
import { Logo } from "../components/Logo/Logo";
import "@testing-library/jest-dom/extend-expect";

describe("Logo", () => {
  it("renders logo with image, subtitle and correct image src", () => {
    const image = "https://example.com/logo.png";
    const subtitle = "My Subtitle";

    render(<Logo image={image} subtitle={subtitle} />);

    const imageElement = screen.getByAltText("logo");
    const subtitleElement = screen.getByText(subtitle);
    const imageSrcAttribute = imageElement.getAttribute("src");

    expect(imageElement).toBeInTheDocument();
    expect(subtitleElement).toBeInTheDocument();
    expect(imageSrcAttribute).toBe(image);
  });
});
