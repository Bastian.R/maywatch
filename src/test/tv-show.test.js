import { TvShowAPI } from "../api/tv-show";

describe("tv-show", () => {
  const objectProperties = [
    "backdrop_path",
    "first_air_date",
    "genre_ids",
    "id",
    "name",
    "origin_country",
    "original_language",
    "original_name",
    "overview",
    "popularity",
    "poster_path",
    "vote_average",
    "vote_count",
  ];
  it("get popular", () => {
    return TvShowAPI.getPopular().then(
      (result) => {
        expect(result.results.length).toBeGreaterThan(0);
        expect(result.results.length).toEqual(20);
        result.results.map((tvShow) => {
          expect(Object.keys(tvShow)).toEqual(
            expect.arrayContaining(objectProperties)
          );
        });
      },
      (rejected) => {
        expect(rejected).toBeInstanceOf(Error);
        expect(rejected.message).toMatch(/Fetch popular request failed/);
      }
    );
  });

  it("fetch tv show by title", () => {
    return TvShowAPI.fetchTvShowByTitle("Blacklist").then(
      (result) => {
        expect(result.results.length).toBeGreaterThan(0);
        result.results.map((tvShow) => {
          expect(Object.keys(tvShow)).toEqual(
            expect.arrayContaining(objectProperties)
          );
        });
      },
      (rejected) => {
        expect(rejected).toBeInstanceOf(Error);
        expect(rejected.message).toMatch(/Fetch tvShowByTitle request failed/);
      }
    );
  });

  it("get recommendations by id", () => {
    return TvShowAPI.getRecommendationsById(1).then(
      (result) => {
        expect(result.results.length).toBeGreaterThan(0);
        result.results.map((tvShow) => {
          expect(Object.keys(tvShow)).toEqual(
            expect.arrayContaining(objectProperties)
          );
        });
      },
      (rejected) => {
        expect(rejected).toBeInstanceOf(Error);
        expect(rejected.message).toMatch(
          /Fetch recommendations request failed./
        );
      }
    );
  });
});
