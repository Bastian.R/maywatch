import {
  render,
  screen,
  waitFor,
  fireEvent,
  hasInputValue,
} from "@testing-library/react";
import App from "../App";
import "@testing-library/jest-dom";

describe("App", () => {
  it("renders waiting message when no current TV show", () => {
    render(<App />);
    const waitingElementTvShow = screen.getByText(
      /waiting to get most current popular tv show/i
    );
    expect(waitingElementTvShow).toBeInTheDocument();
  });
  it("renders waiting message when no current recommendations", () => {
    render(<App />);
    const waitingElementRecommendation = screen.getByText(
      /waiting to get best recommendations/i
    );
    expect(waitingElementRecommendation).toBeInTheDocument();
  });
  it("renders basic structure", () => {
    render(<App />);
    const mainContainer = screen.getByTestId("main-container-test");
    const header = screen.getByTestId("header-test");
    const logo = screen.getByTestId("logo-test");
    const searchBar = screen.getByTestId("searchBar-test");
  });

  it("renders elements waiting for promise", async () => {
    render(<App />);
    const tvShowDetail = await screen.findByTestId("tvShowDetail-test");
    const recommendations = await screen.findByTestId("recommendations");
  });

  //another way to do it using waitforFunction
  //wait for elements appear & instructions to be executed
  //   it("renders elements waiting for promise", () => {
  //     waitFor(() => {
  //       const tvShowDetail = screen.findByTestId("tvShowDetail-test");
  //       const recommendations = screen.findByTestId("recommendations");
  //     });
  //   });

  it("back to first element of recommendations when clicking on a tv show in recommendations", async () => {
    render(<App />);
    const backStartHorizontalScroll = jest.fn();
    const lastRecommendationItem = await screen.findByTestId(
      "recommendations-tv-show-item-9"
    );
    fireEvent.click(lastRecommendationItem, backStartHorizontalScroll());
    expect(backStartHorizontalScroll).toHaveBeenCalledTimes(1);
  });

  it("function setCurrentTvShowChildBySearch", () => {
    render(<App />);
    const searchBarInput = screen.getByTestId("searchBar-input-test");

    const objectExample = {
      backdrop_path: "/mWbMxfpQsY9bY3TB4Tj1J2FgboM.jpg",
      first_air_date: "2013-09-23",
      genre_ids: [80, 18, 9648],
      id: 46952,
      name: "The Blacklist",
      origin_country: ["US"],
      original_language: "en",
      original_name: "The Blacklist",
      overview: "",
      popularity: 123.72,
      poster_path: "/bgbQCW4fE9b6wSOSC6Fb4FfVzsW.jpg",
      vote_average: 7.4,
      vote_count: 1728,
    };
    const requestFailed = jest
      .fn()
      .mockImplementationOnce(() =>
        Promise.reject(new Error("Fetch tv request failed."))
      );
    const requestSuccess = jest
      .fn()
      .mockImplementationOnce(() => Promise.resolve(objectExample));
    //We could simulate the state instead of fake function https://medium.com/developer-rants/testing-the-react-component-state-with-jest-b0a072f70f44
    //But we know that the useState hook & useEffect hook are working well cause of verifications of apparition of async elements in previous tests
    const setCurrentTvShow = jest.fn((object) => {
      console.log(object);
    });
    const alert = jest.spyOn(window, "alert").mockImplementation(() => {});
    const fetchTvShowByTitle = [requestFailed, requestSuccess];

    const setCurrentTvShowChildBySearch = jest.fn((promise) => {
      promise.then(
        (result) => {
          setCurrentTvShow(result);
          searchBarInput.value = "";
          expect(setCurrentTvShow).toBeCalledTimes(1);
          expect(searchBarInput.value).toBe("");
        },
        (reject) => {
          alert(reject.message);
          expect(alert).toBeCalledTimes(1);
          expect(reject).toBeInstanceOf(Error);
        }
      );
    });

    fetchTvShowByTitle.map((result) => {
      setCurrentTvShowChildBySearch(result());
    });
  });
});
