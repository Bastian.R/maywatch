import {
  render,
  screen,
  fireEvent,
  shallow,
  getByTestId,
} from "@testing-library/react";
import { TvShowListItem } from "../components/TvShowListItem/TvShowListItem";
import "@testing-library/jest-dom";
import { SMALL_IMG_COVER_BASE_URL } from "../config.js";
import imgNoPreview from "../assets/images/no-preview.jpg";

describe("TvShowListItem", () => {
  it("", () => {
    const recommendations = [
      {
        adult: false,
        backdrop_path: "/2MaumbgBlW1NoPo3ZJO38A6v7OS.jpg",
        id: 66732,
        name: "Stranger Things",
        original_language: "en",
        original_name: "Stranger Things",
        overview:
          "When a young boy vanishes, a small town uncovers a mystery involving secret experiments, terrifying supernatural forces, and one strange little girl.",
        poster_path: "/49WJfeN0moxb9IPfGn8AIqMGskD.jpg",
        media_type: "tv",
        genre_ids: [18, 10765, 9648],
        popularity: 233.142,
        first_air_date: "2016-07-15",
        vote_average: 8.625,
        vote_count: 16137,
        origin_country: ["US"],
      },
      {
        adult: false,
        backdrop_path: "/wZMY9X8jtSS5GXFue2lvhgaJkii.jpg",
        id: 42009,
        name: "Black Mirror",
        original_language: "en",
        original_name: "Black Mirror",
        overview:
          "Over the last ten years, technology has transformed almost every aspect of our lives before we've had time to stop and question it. In every home; on every desk; in every palm - a plasma screen; a monitor; a smartphone - a black mirror of our 21st Century existence.",
        poster_path: "/5UaYsGZOFhjFDwQh6GuLjjA1WlF.jpg",
        media_type: "tv",
        genre_ids: [10765, 18, 9648],
        popularity: 213.842,
        first_air_date: "2011-12-04",
        vote_average: 8.306,
        vote_count: 4372,
        origin_country: ["GB"],
      },
      {
        adult: false,
        backdrop_path: "/tsRy63Mu5cu8etL1X7ZLyf7UP1M.jpg",
        id: 1396,
        name: "Breaking Bad",
        original_language: "en",
        original_name: "Breaking Bad",
        overview:
          "When Walter White, a New Mexico chemistry teacher, is diagnosed with Stage III cancer and given a prognosis of only two years left to live. He becomes filled with a sense of fearlessness and an unrelenting desire to secure his family's financial future at any cost as he enters the dangerous world of drugs and crime.",
        poster_path: "/3xnWaLQjelJDDF7LT1WBo6f4BRe.jpg",
        media_type: "tv",
        genre_ids: [18, 80],
        popularity: 283.486,
        first_air_date: "2008-01-20",
        vote_average: 8.889,
        vote_count: 12357,
        origin_country: ["US"],
      },
      {
        adult: false,
        backdrop_path: "/2OMB0ynKlyIenMJWI2Dy9IWT4c.jpg",
        id: 1399,
        name: "Game of Thrones",
        original_language: "en",
        original_name: "Game of Thrones",
        overview:
          "Seven noble families fight for control of the mythical land of Westeros. Friction between the houses leads to full-scale war. All while a very ancient evil awakens in the farthest north. Amidst the war, a neglected military order of misfits, the Night's Watch, is all that stands between the realms of men and icy horrors beyond.",
        poster_path: "/1XS1oqL89opfnbLl8WnZY1O1uJx.jpg",
        media_type: "tv",
        genre_ids: [10765, 18, 10759],
        popularity: 549.842,
        first_air_date: "2011-04-17",
        vote_average: 8.441,
        vote_count: 21815,
        origin_country: ["US"],
      },
      {
        adult: false,
        backdrop_path: "/scx72FpTF929jYXea0MphyE24Wa.jpg",
        id: 46648,
        name: "True Detective",
        original_language: "en",
        original_name: "True Detective",
        overview:
          "An American anthology police detective series utilizing multiple timelines in which investigations seem to unearth personal and professional secrets of those involved, both within or outside the law.",
        poster_path: "/aowr4xpLP5sRCL50TkuADomJ98T.jpg",
        media_type: "tv",
        genre_ids: [18],
        popularity: 168.401,
        first_air_date: "2014-01-12",
        vote_average: 8.3,
        vote_count: 2777,
        origin_country: ["US"],
      },
      {
        adult: false,
        backdrop_path: "/ex4kvQb0Ski5KL2fsnKQU2hV3oo.jpg",
        id: 1425,
        name: "House of Cards",
        original_language: "en",
        original_name: "House of Cards",
        overview:
          "Set in present day Washington, D.C., House of Cards is the story of Frank Underwood, a ruthless and cunning politician, and his wife Claire who will stop at nothing to conquer everything. This wicked political drama penetrates the shadowy world of greed, sex and corruption in modern D.C.",
        poster_path: "/hKWxWjFwnMvkWQawbhvC0Y7ygQ8.jpg",
        media_type: "tv",
        genre_ids: [18],
        popularity: 81.281,
        first_air_date: "2013-02-01",
        vote_average: 8.044,
        vote_count: 2399,
        origin_country: ["US"],
      },
      {
        adult: false,
        backdrop_path: "/r0Q6eeN9L1ORL9QsV0Sg8ZV3vnv.jpg",
        id: 1408,
        name: "House",
        original_language: "en",
        original_name: "House",
        overview:
          "Dr. Gregory House, a drug-addicted, unconventional, misanthropic medical genius, leads a team of diagnosticians at the fictional Princeton–Plainsboro Teaching Hospital in New Jersey.",
        poster_path: "/3Cz7ySOQJmqiuTdrc6CY0r65yDI.jpg",
        media_type: "tv",
        genre_ids: [18, 9648],
        popularity: 425.76,
        first_air_date: "2004-11-16",
        vote_average: 8.595,
        vote_count: 5789,
        origin_country: ["US"],
      },
      {
        adult: false,
        backdrop_path: "/wiE9doxiLwq3WCGamDIOb2PqBqc.jpg",
        id: 60574,
        name: "Peaky Blinders",
        original_language: "en",
        original_name: "Peaky Blinders",
        overview:
          "A gangster family epic set in 1919 Birmingham, England and centered on a gang who sew razor blades in the peaks of their caps, and their fierce boss Tommy Shelby, who means to move up in the world.",
        poster_path: "/vUUqzWa2LnHIVqkaKVlVGkVcZIW.jpg",
        media_type: "tv",
        genre_ids: [18, 80],
        popularity: 347.34,
        first_air_date: "2013-09-12",
        vote_average: 8.548,
        vote_count: 8821,
        origin_country: ["GB"],
      },
      {
        adult: false,
        backdrop_path: "/FtK35pwgGHTFu8hySVlFmfF8td.jpg",
        id: 60622,
        name: "Fargo",
        original_language: "en",
        original_name: "Fargo",
        overview:
          "A close-knit anthology series dealing with stories involving malice, violence and murder based in and around Minnesota.",
        poster_path: "/9ZIhl17uFlXCNUputSEDHwZYIEJ.jpg",
        media_type: "tv",
        genre_ids: [80, 18],
        popularity: 169.054,
        first_air_date: "2014-04-15",
        vote_average: 8.312,
        vote_count: 2272,
        origin_country: ["US"],
      },
      {
        adult: false,
        backdrop_path: "/aSGSxGMTP893DPMCvMl9AdnEICE.jpg",
        id: 1405,
        name: "Dexter",
        original_language: "en",
        original_name: "Dexter",
        overview:
          "Dexter Morgan, a blood spatter pattern analyst for the Miami Metro Police also leads a secret life as a serial killer, hunting down criminals who have slipped through the cracks of justice.",
        poster_path: "/q8dWfc4JwQuv3HayIZeO84jAXED.jpg",
        media_type: "tv",
        genre_ids: [80, 18, 9648],
        popularity: 172.342,
        first_air_date: "2006-10-01",
        vote_average: 8.19,
        vote_count: 3667,
        origin_country: ["US"],
      },
    ];
    const onClick = jest.fn();

    render(
      <TvShowListItem recommendations={recommendations} onClick={onClick} />
    );

    const spanTitle = screen.getByText(/You will also like/);

    const tvShowList = screen.getAllByTestId(
      /recommendations-tv-show-item-[0-9]{1}/
    );

    recommendations.map((e, index) => {
      let span = screen.getByText(e.name);
      let cover = screen.getByAltText(`tv-show-cover-${index}`);
      expect(cover.getAttribute("src")).toBe(
        e.backdrop_path
          ? SMALL_IMG_COVER_BASE_URL + e.backdrop_path
          : imgNoPreview
      );
    });

    expect(tvShowList).toHaveLength(10);

    tvShowList.map((e, index) => {
      fireEvent.click(e);
      fireEvent.mouseEnter(e);
      fireEvent.mouseLeave(e);
      expect(onClick).toHaveBeenCalledTimes(index + 1);
    });
  });
});
